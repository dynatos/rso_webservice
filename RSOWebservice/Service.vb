﻿Imports ReadSoft.Services.Client
Imports ReadSoft.Services.Client.Entities
Imports System.Timers
Imports System.IO

Public Class Service

    Dim _config

    Dim _clientConfig As ClientConfiguration
    Dim _docClient As DocumentServiceClient
    Dim _userClient As UserServiceClient
    Dim _accClient As AccountServiceClient
    Dim _authClient As AuthenticationServiceClient
    Dim _mdClient As MasterDataServiceClient
    Dim _credentials As AuthenticationCredentials

    Dim _documents As DocumentReferenceCollection
    Dim _authenticationResult As AuthenticationResult
    Dim _currentuser, Credentials
    Dim _eventlog As New EventLog
    Dim _outputresult As OutputResult

    Protected Overrides Sub OnStart(ByVal args() As String)
        Dim timer As New Timer
        AddHandler timer.Elapsed, AddressOf OnTimedEvent

        _eventlog = New EventLog
        _eventlog.Source = My.Settings.eventlogSource

        login()
        timer.Interval = 60000
        timer.Enabled = True
        timer.Start()

        writeInfoAudit("Dynatos Webservice v1.0 Started")
    End Sub

    Protected Overrides Sub OnStop()
        writeInfoAudit("Dynatos Webservice v1.0 Stopped")
    End Sub
    Private Sub login()
        Try
            _clientConfig = ClientConfiguration.Create(Guid.Parse(My.Settings.ApiKey), New Uri(My.Settings.ServiceUri))

            _credentials = New AuthenticationCredentials
            _credentials.AuthenticationType = Entities.AuthenticationType.SetCookie
            _credentials.UserName = My.Settings.username
            _credentials.Password = My.Settings.password

            _authClient = New AuthenticationServiceClient(_clientConfig)

            _authenticationResult = _authClient.Authenticate(_credentials)

            If _authenticationResult.Status = AuthenticationStatus.Success Then
                _docClient = New DocumentServiceClient(_clientConfig)
                _userClient = New UserServiceClient(_clientConfig)
                _accClient = New AccountServiceClient(_clientConfig)
                _mdClient = New MasterDataServiceClient(_clientConfig)
                EventLog.WriteEntry("Successfully logged in as " & _credentials.UserName & "!")
            Else
                EventLog.WriteEntry("Error while logging in!")
            End If
        Catch ex As Exception
            EventLog.WriteEntry("Error while logging in!" & vbNewLine & ex.Message)
        End Try

    End Sub

    Private Sub OnTimedEvent(ByVal sender As Object, ByVal e As System.EventArgs)
        _documents = _docClient.GetOutputDocumentsByCurrentCustomer

        Dim _currentdoc As New Document

        If _documents.Count < 0 Then
            writeInfoAudit("Processing " & _documents.Count & " invoices.")
        End If

        For Each ref As DocumentReference In _documents
            Try
                _outputresult = New OutputResult
                _outputresult.Status = OutputStatus.Success
                _outputresult.Message = "Document Registered"

                _currentdoc = _docClient.GetDocument(ref.DocumentId)

                Dim buyerparty As Party = getParty(_currentdoc, "buyer")
                Dim supplierparty As Party = getParty(_currentdoc, "supplier")

                Dim buyerid As String = getBuyerId(buyerparty.Name)
                Dim captured_iban As String = getHeaderField(_currentdoc, "IBAN").Text
                Dim captured_vat As String = getHeaderField(_currentdoc, "").Text

                'CHECK IF CAPTURED VAT & IBAN MATCH THE MASTERDATA
                Dim isCorrectIBAN As Boolean = isCorrectIBANforSupplier(buyerid, supplierparty.Id, captured_iban)
                Dim isCorrectVAT As Boolean = isCorrectVATforSupplier(captured_vat, supplierparty.TaxRegistrationNumber)

                'UPDATE OUTPUTRESULT BASED ON VALIDATIONS
                If Not isCorrectIBAN Then
                    _outputresult.Status = OutputStatus.Failure
                    _outputresult.Message = "IBAN " & captured_iban & " was not found in the supplier masterdata."
                End If

                If Not isCorrectVAT Then
                    If _outputresult.Status = OutputStatus.Failure Then
                        _outputresult.Message += vbNewLine & "VAT " & captured_vat & " was not found int he supplier masterdata."
                    Else
                        _outputresult.Status = OutputStatus.Failure
                        _outputresult.Message = "VAT " & captured_vat & " was not found int he supplier masterdata."
                    End If
                End If

                _docClient.DocumentStatus(_currentdoc.Id, _outputresult)
            Catch ex As Exception
                writeErrorAudit("ERROR in Timed Event for document: " & _currentdoc.TrackId & vbNewLine & ex.Message)
            End Try
        Next

    End Sub

    '==============================================
    '=               PRIVATE FUNCTIONS            =
    '==============================================
    Private Sub writeSuccessAudit(message As String)
        Try
            _eventlog.WriteEntry(message, EventLogEntryType.SuccessAudit)
        Catch ex As Exception

        End Try

    End Sub
    Private Sub writeErrorAudit(message As String)
        Try
            _eventlog.WriteEntry(message, EventLogEntryType.Error)
        Catch ex As Exception

        End Try

    End Sub
    Private Sub writeInfoAudit(message As String)
        Try
            _eventlog.WriteEntry(message, EventLogEntryType.Warning)
        Catch ex As Exception

        End Try

    End Sub
    Private Function getParty(doc As Document, partyType As String) As Party
        Dim party = Nothing

        For Each p As Party In doc.Parties
            If p.Type.ToLower = partyType.ToLower Then
                party = p
            End If
        Next
        Return party
    End Function
    Private Function getHeaderField(doc As Document, fieldTypeName As String) As HeaderField
        Dim headerfield = Nothing

        For Each h As HeaderField In doc.HeaderFields
            If h.Type.ToLower = fieldTypeName.ToLower Then
                headerfield = h
            End If
        Next

        Return headerfield
    End Function
    Private Function getCodingField(codingLine As CodingLine, fieldTypeName As String) As CodingField
        Dim codingfield = Nothing
        For Each c As CodingField In codingLine.CodingFields
            If c.Type.ToLower = fieldTypeName.ToLower Then
                codingfield = c
            End If
        Next

        Return codingfield
    End Function
    Private Function getLineItemTable(document As Document) As Table
        Dim linetable = Nothing

        For Each t As Table In document.Tables
            If t.Type = "LineItem" Then
                linetable = t
            End If
        Next

        Return linetable
    End Function
    Private Function getItemfield(tablerow As TableRow, field As String) As ItemField
        Dim itemfield = Nothing

        For Each i As ItemField In tablerow.ItemFields
            If i.Name.ToLower = field.ToLower Then
                itemfield = i
            End If
        Next

        Return itemfield
    End Function
    Private Function isApproved(doc As Document) As Boolean
        For Each h As DocumentHistoryItem In doc.History
            If h.Status = DocumentStatus.DocumentApprovalSucceeded Then
                Return True
            End If
        Next
        Return False
    End Function
    Private Function getApprover(document As Document) As String
        Dim approver As String = String.Empty

        For Each item As DocumentHistoryItem In document.History
            If item.Status = DocumentStatus.DocumentApproved Then
                approver = item.UserFullName.ToString
            End If
        Next

        Return approver
    End Function
    Private Function isCredit(doc As Document) As Boolean
        Dim credit As Boolean = False

        If getHeaderField(doc, "creditinvoice").Text = True Then
            credit = True
        End If

        Return credit
    End Function
    Private Function clearStringExotic(input As String) As String
        Dim illegalChars As Char() = "!@`´#$%^/:\&*|{}[]""'_+<>?".ToCharArray()

        Dim sb As New System.Text.StringBuilder

        For Each ch As Char In input
            If Array.IndexOf(illegalChars, ch) = -1 Then
                sb.Append(ch)
            End If
        Next
        Return sb.ToString
    End Function
    Private Function clearStringExact(input As String, removeCharacters As String) As String
        Dim illegalChars As Char() = removeCharacters.ToCharArray()

        Dim sb As New System.Text.StringBuilder

        For Each ch As Char In input
            If Array.IndexOf(illegalChars, ch) = -1 Then
                sb.Append(ch)
            End If
        Next

        Return sb.ToString
    End Function

    Private Function isFilledin(input As String) As Boolean

        If String.IsNullOrEmpty(input) Or input.Equals("") Then
            Return False
        Else
            Return True
        End If

    End Function
    Private Function getBuyerId(buyername As String) As String
        Dim buyerid As String = ""

        Dim custos As Customer = _accClient.GetCurrentCustomer()
        Dim buyers As BuyerCollection = _accClient.GetAllBuyersByCustomer(custos.Id)

        For Each b In buyers
            If b.Name.Equals(buyername) Then
                buyerid = b.Id
            End If
        Next

        Return buyerid
    End Function
    Private Function isCorrectIBANforSupplier(buyerid As String, supplierid As String, iban As String) As Boolean
        Dim result As Boolean = False
        Try
            Dim supplierbanks As New SupplierBankAccountCollection

            supplierbanks = _mdClient.GetSupplierBanksForSingleSupplier(buyerid, supplierid)

            For Each bank In supplierbanks
                If clearStringExact(bank.AccountNumber, "-").Equals(iban) Then
                    result = True
                    Exit For
                End If
            Next

            If supplierbanks.Count = 0 And Not isFilledin(iban) Then
                result = True
            End If

            Return result
        Catch ex As Exception
            Return False
        End Try
    End Function
    Private Function isCorrectVATforSupplier(capturedvatnumber As String, mdvatnumber As String) As Boolean
        If capturedvatnumber.Equals(mdvatnumber) Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Function createStandardOutputXML(doc As Document) As String
        Dim path As String = ""
        Try

            Dim format As DocumentFormat = DocumentFormat.XmlV2
            Dim md As MetaDataCollection = New MetaDataCollection
            Dim Stream As Stream = _docClient.GetDocumentOutputData(doc.Id, md)

            Dim buyerparty As Party = getParty(doc, "buyer")

            Dim archivefile As String = My.Settings.archivePath & "\" & buyerparty.Name & "\" & doc.DocumentType & "\" & doc.Id & ".xml"
            Dim outputfile As String = My.Settings.xmlLocation & "\" & buyerparty.Name & "\" & doc.DocumentType & "\" & doc.Id & ".xml"

            Directory.CreateDirectory(My.Settings.archivePath & "\" & buyerparty.Name & "\" & doc.DocumentType)

            WriteStreamtoFile(Stream, archivefile)

            If File.Exists(archivefile) Then
                Directory.CreateDirectory(My.Settings.xmlLocation & "\" & buyerparty.Name & "\" & doc.DocumentType)
                File.Copy(archivefile, outputfile)
                Return archivefile
            End If
        Catch ex As Exception
            Return ""
        End Try
        Return path
    End Function
    Private Sub WriteStreamtoFile(stream As Stream, file As String)
        Try
            Dim bytesRead As Integer
            Dim buffer(4096) As Byte
            Using stream
                Using outFile As New FileStream(file, IO.FileMode.Create, IO.FileAccess.Write)
                    Do
                        bytesRead = stream.Read(buffer, 0, buffer.Length)
                        If bytesRead > 0 Then
                            outFile.Write(buffer, 0, bytesRead)
                        End If
                    Loop While bytesRead > 0
                End Using
            End Using
        Catch ex As Exception
            'MsgBox(ex.Message)
            writeErrorAudit("Error while writing to stream:" & vbNewLine & ex.Message)
        End Try
    End Sub
    Private Function downloadDocumentImage(doc As Document, name As String) As String

        Dim pathSource As Object = _docClient.GetDocumentOutputImage(doc.Id)

        Dim buyerparty As Party = getParty(doc, "buyer")
        Directory.CreateDirectory(My.Settings.archivePath & "\" & buyerparty.Name & "\" & doc.DocumentType)

        Dim pathNew As String = My.Settings.archivePath & "\" & buyerparty.Name & "\" & doc.DocumentType & "\" & name & ".pdf"
        Dim pdfoutput As String = My.Settings.pdfLocation & "\" & buyerparty.Name & "\" & doc.DocumentType & "\" & name & ".pdf"

        ' Read the source file into a byte array.
        Dim bytes() As Byte = New Byte((pathSource.Length) - 1) {}
        Dim numBytesToRead As Integer = CType(pathSource.Length, Integer)
        Dim numBytesRead As Integer = 0
        Try
            While (numBytesToRead > 0)
                ' Read may return anything from 0 to numBytesToRead.
                Dim n As Integer = pathSource.Read(bytes, numBytesRead,
                        numBytesToRead)
                ' Break when the end of the file is reached.
                If (n = 0) Then
                    Exit While
                End If
                numBytesRead = (numBytesRead + n)
                numBytesToRead = (numBytesToRead - n)

            End While
            numBytesToRead = bytes.Length

            ' Write the byte array to the other FileStream.

            Using fsNew As FileStream = New FileStream(pathNew,
                        FileMode.Create, FileAccess.Write)
                fsNew.Write(bytes, 0, numBytesToRead)
            End Using

            If File.Exists(pathNew) Then
                Directory.CreateDirectory(My.Settings.pdfLocation & "\" & buyerparty.Name & "\" & doc.DocumentType)
                File.Copy(pathNew, pdfoutput)
                writeSuccessAudit("Document downloaded to " & pathNew.ToString)
                Return pathNew
            End If

        Catch ex As Exception
            writeErrorAudit("ERROR WHILE DOWNLOADING PDF DOCUMENT" & vbNewLine & ex.Message)
        End Try
        Return ""
    End Function
End Class