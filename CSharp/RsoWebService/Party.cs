﻿namespace RsoWebService
{
    public class Party
    {
        public string Type;
        public string Name;
        public string ExternalId;
        public string Description;
        public string TaxRegistrationNumber;
        public string OrganizationNumber;
        public string Street;
        public string PostalCode;
        public string City;
        public string CountryName;
        public string PaymentTerm;
        public string PaymentMethod;
        public string CurrencyCode;
        public string BankAccounts;
        public string ValidationError;
        public string Location;
        public string State;
        public string Blocked;
        public string TelephoneNumber;
        public string FaxNumber;
        public string TaxCode;
    }
}