﻿namespace RsoWebService
{
    public class History
    {
        public string Timestamp;
        public string UserFullName;
        public string EntryType;
        public string Status;
        public string Comment;
        public string Version;
        public string ChangeReportAvailable;
        public string StatusAsInt;
    }
}
