﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using Newtonsoft.Json;

namespace RsoWebService
{
    class Program
    {
        static void Main()
        {
            try
            {
                Authenticate(); //Login and create cookie for further use
                foreach (var outputDocument in GetDocuments())
                {
                    var document = new Document();
                    try
                    {
                        document = GetDocument(outputDocument.DocumentId);
                        document = CodingLineDetermination(document);
                        document.OutputResult = new OutputResult { Status = 0, Message = "Dynatos TEST", CodingLines = document.AccountingInformation.CodingLines };
                    }
                    catch (Exception e)
                    {
                         document.OutputResult = new OutputResult { Status = 0, Message = e.Message, CodingLines = document.AccountingInformation.CodingLines };
                    }

                    UpdateDocumentStatus(document); //Update the output result.
                }
            }
            catch (WebException ex)
            {
                using (var stream = ex.Response.GetResponseStream())
                using (var reader = new StreamReader(stream ?? throw new InvalidOperationException("No Stream.")))
                {
                    Console.WriteLine(reader.ReadToEnd());
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            Console.ReadKey();
        }

        private static void UpdateDocumentStatus(Document document)
        {
            var client = (HttpWebRequest)WebRequest.Create(DocUri + $"{document.Id}/documentstatus");
            client.Accept = "application/json";
            client.ContentType = "application/json";
            client.Headers.Add("x-rs-version", "2011-10-14");
            client.Headers.Add("x-rs-key", "7f3b4b38e9c141bfa84a6d9ecabb9d72");
            client.Headers.Add("x-rs-culture", CultureInfo.CurrentCulture.Name);
            client.Headers.Add("x-rs-uiculture", CultureInfo.CurrentUICulture.Name);
            client.CookieContainer = new CookieContainer();
            client.CookieContainer.Add(_currentCookie);
            client.Method = "PUT";

            var json = JsonConvert.SerializeObject(document.OutputResult);

            var encoding = new ASCIIEncoding();
            var data = encoding.GetBytes(json);
            client.ContentLength = data.Length;
            var newStream = client.GetRequestStream();
            // Send the data.
            newStream.Write(data, 0, data.Length);
            newStream.Close();


            using (var response = client.GetResponse())
            {
                using (var resp = response.GetResponseStream())
                {
                    if (resp == null) return;
                    using (var sr = new StreamReader(resp, Encoding.UTF8))
                    {
                        Console.WriteLine(sr.ReadToEnd());
                    }
                }
            }


        }

        private static Document GetDocument(string documentId)
        {
            var client = (HttpWebRequest)WebRequest.Create(DocUri + documentId);
            client.Accept = "application/json";
            client.ContentType = "application/json";
            client.Headers.Add("x-rs-version", "2011-10-14");
            client.Headers.Add("x-rs-key", "7f3b4b38e9c141bfa84a6d9ecabb9d72");
            client.Headers.Add("x-rs-culture", CultureInfo.CurrentCulture.Name);
            client.Headers.Add("x-rs-uiculture", CultureInfo.CurrentUICulture.Name);

            client.CookieContainer = new CookieContainer();
            client.CookieContainer.Add(_currentCookie);
            client.Method = "GET";

            using (var response = client.GetResponse())
            {
                using (var resp = response.GetResponseStream())
                {
                    if (resp == null) return new Document();
                    using (var sr = new StreamReader(resp, Encoding.UTF8))
                    {
                        return JsonConvert.DeserializeObject<Document>(sr.ReadToEnd());
                    }
                }
            }
        }

        /// <summary>
        /// Gets the output documents
        /// </summary>
        private static IEnumerable<OutputDocument> GetDocuments()
        {
            var client = (HttpWebRequest)WebRequest.Create(DocUri+ "customers/outputdocumentsbycurrentcustomer");
            client.Accept = "application/json";
            client.ContentType = "application/json";
            client.Headers.Add("x-rs-version", "2011-10-14");
            client.Headers.Add("x-rs-key", "7f3b4b38e9c141bfa84a6d9ecabb9d72");
            client.Headers.Add("x-rs-culture", CultureInfo.CurrentCulture.Name);
            client.Headers.Add("x-rs-uiculture", CultureInfo.CurrentUICulture.Name);

            client.CookieContainer = new CookieContainer();
            client.CookieContainer.Add(_currentCookie);
            client.Method = "GET";

            using (var response = client.GetResponse())
            {
                using (var resp = response.GetResponseStream())
                {
                    if (resp == null) return new List<OutputDocument>();
                    using (var sr = new StreamReader(resp, Encoding.UTF8))
                    {
                        return JsonConvert.DeserializeObject<List<OutputDocument>> (sr.ReadToEnd());
                    }
                }
            }
        }
        /// <summary>
        /// Authentication for the RSO. Creates cookie and set the global parameter so it can be used in other calls for authorization purposes.
        /// </summary>
        private static void Authenticate()
        {
            try
            {
                var client = (HttpWebRequest) WebRequest.Create(AuthUri);

                client.CookieContainer = new CookieContainer();
                client.Accept = "application/xml";
                client.ContentType = "application/xml";
                client.Headers.Add("x-rs-version", "2011-10-14");
                client.Headers.Add("x-rs-key", "7f3b4b38e9c141bfa84a6d9ecabb9d72");
                client.Headers.Add("x-rs-culture", CultureInfo.CurrentCulture.Name);
                client.Headers.Add("x-rs-uiculture", CultureInfo.CurrentUICulture.Name);
                client.Method = "POST";

                using (var request = client.GetRequestStream())
                {
                    using (var sr = new StreamWriter(request, Encoding.UTF8))
                    {
                        sr.Write(
                            "<AuthenticationCredentials><UserName>produo.readsoftconnect</UserName><Password>-Dynat0s-</Password><AuthenticationType>SetCookie</AuthenticationType></AuthenticationCredentials>"
                            //"{\n  \"UserName\": \"produo.readsoftconnect\",\n  \"Password\": \"-Dynat0s-\",\n  \"AuthenticationType\": \"SetCookie\"\n}"
                            //"{'UserName':produo.readsoftconnect'Password':-Dynat0s-,'AuthenticationType':0}"
                            );
                    }
                }

                using (var response = client.GetResponse())
                {
                    using (var resp = response.GetResponseStream())
                    {
                        if (resp != null)
                            using (var sr = new StreamReader(resp, Encoding.UTF8))
                            {
                                System.Diagnostics.Debug.WriteLine(sr.ReadToEnd());
                            }
                    }
                }

                if (client.CookieContainer.Count <= 0) return;
                _currentCookie = client.CookieContainer.GetCookies(AuthUri)[0];
                Console.WriteLine("Authenticated.");
            }
            catch (Exception e)
            {
                if (Debug) Console.WriteLine(e);
                throw new Exception(e.ToString());
            }
            //Try
            //    _clientConfig = ClientConfiguration.Create(Guid.Parse(My.Settings.ApiKey), New Uri(My.Settings.ServiceUri))

            //_credentials = New AuthenticationCredentials
            //_credentials.AuthenticationType = Entities.AuthenticationType.SetCookie
            //_credentials.UserName = My.Settings.username
            //_credentials.Password = My.Settings.password

            //_authClient = New AuthenticationServiceClient(_clientConfig)

            //_authenticationResult = _authClient.Authenticate(_credentials)

            //If _authenticationResult.Status = AuthenticationStatus.Success Then
            //    _docClient = New DocumentServiceClient(_clientConfig)
            //_userClient = New UserServiceClient(_clientConfig)
            //_accClient = New AccountServiceClient(_clientConfig)
            //_mdClient = New MasterDataServiceClient(_clientConfig)
            //EventLog.WriteEntry("Successfully logged in as " & _credentials.UserName & "!")
            //Else
            //EventLog.WriteEntry("Error while logging in!")
            //End If
            //Catch ex As Exception
            //EventLog.WriteEntry("Error while logging in!" & vbNewLine & ex.Message)
            //End Try

        }
        /// <summary>
        ///     If one codingline exists go to SQL Table for determination. Based on Buyer(externalId / Organization number) and Supplier.
        /// 
        ///     If CostCenter is numeric > Supplier has 1 codingline
        ///         Take GL-Account, CostCenter, BTW-Code from SQL-table
        ///         Take other fields from the header
        /// 
        ///     If CostCenter is non-numeric > Supplier has multiple codinglines
        ///    
        ///     If one codingline and data is provided (GL-Account, CostCenter, BTW-Code) and it doesn't match the table leave the data.
        ///     If no records were found in the table > pending correction
        ///     If multiple records were found in the table > Create amount (amount and VAT) of records and split amount in the newly created codinglines
        ///     If Accrual is filled then start date should be filled too. If not > pending correction
        ///     If result of fields (GL-Account, CostCenter, BTW-Code) after table search are empty > pending correction
        /// </summary>
        public static Document CodingLineDetermination(Document doc)
        {
            var codingLines = doc.AccountingInformation.CodingLines;
            var codingLinesSql = GetCodingLinesFromDb(doc);

            if (codingLines.Count <= 0) throw new Exception("No lines found one should be created on the document.");
            foreach (var codingLine in codingLines)
            {
                var costCenterDoc = codingLine.CostCenterCode;
                var glAccountDoc = codingLine.AccountCode;
                var vatCodeDoc = codingLine.CodingFields.First(cf => cf.Type.Equals("TaxCode"));

                if (codingLinesSql.Count <= 0) throw new Exception("No lines found in the SQL-tables.");
                if (codingLinesSql.Count == 1)
                {
                    var costCenter = string.Empty;
                    codingLinesSql.ForEach(c =>
                    {
                        var costCenterSql = c.CodingFields.First(f => f.Name.Equals("CostCenter"));
                        //var glAccountSql = c.CodingFields.First(f => f.Name.Equals("GLaccount"));
                        var vatCodeSql = c.CodingFields.First(f => f.Name.Equals("VATCode"));

                        if (int.TryParse(costCenterSql.Text, out var _))
                        {
                            if (!string.IsNullOrEmpty(costCenterSql.Text))
                            {
                                if (string.IsNullOrEmpty(costCenterDoc))
                                    codingLine.CodingFields.First(cf => cf.Type.Equals("CostCenter")).Text = costCenterSql.Text;
                            }
                            else
                            {
                                throw new Exception("CostCenter was empty in the database.");
                            }

                            //if (!string.IsNullOrEmpty(glAccountSql.Text))
                            //{
                            //    if (string.IsNullOrEmpty(glAccountDoc))
                            //        codingLine.CodingFields.First(cf => cf.Type.Equals("GLaccount")).Text = costCenterSql.Text;
                            //}
                            //else
                            //{
                            //    throw new Exception("CostCenter was empty in the database.");
                            //}


                            if (!string.IsNullOrEmpty(vatCodeSql.Text))
                            {
                                if (string.IsNullOrEmpty(vatCodeDoc.Text))
                                    codingLine.CodingFields.First(cf => cf.Type.Equals("TaxCode")).Text = costCenterSql.Text;
                            }
                            else
                            {
                                throw new Exception("CostCenter was empty in the database.");
                            }
                        }
                        else
                        {
                            throw new Exception($"CostCenter should be found in another table {costCenter}.");
                        }
                    });
                }
                else
                {
                    //Multiple lines
                    throw new Exception("Multiple lines will be handled later.");
                }
            }
            return doc;
        }

        public static List<CodingLine> GetCodingLinesFromDb(Document doc)
        {
            var buyer = GetBuyer(doc);
            var supplier = GetSupplier(doc);
            var codingLines = new List<CodingLine>();
            using (var conn = new SqlConnection("Server=WS12-READSOFT\\READSOFT;Database=Development;Uid=sa;Pwd = -Dynat0s-; "))
            {
                var cmd = new SqlCommand($"SELECT *FROM [dbo].[xxdyn_accountingTable] where Buyer = '{buyer.ExternalId}' And SupplierNumber = '{supplier.ExternalId}'", conn);
                try
                {
                    conn.Open();
                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        var codingLine = new CodingLine();
                        codingLine.CodingFields.Add(new CodingField
                        {
                            Name = "Buyer",
                            Text = reader["Buyer"].ToString()
                        });

                        codingLine.CodingFields.Add(new CodingField
                        {
                            Name = "SupplierNumber",
                            Text = reader["SupplierNumber"].ToString()
                        });
                        codingLine.CodingFields.Add(new CodingField
                        {
                            Name = "SupplierName",
                            Text = reader["SupplierName"].ToString()
                        });
                        codingLine.CodingFields.Add(new CodingField
                        {
                            Name = "GLaccount",
                            Text = reader["GLaccount"].ToString()
                        });
                        codingLine.CodingFields.Add(new CodingField
                        {
                            Name = "CostCenter",
                            Text = reader["CostCenter"].ToString()
                        });
                        codingLine.CodingFields.Add(new CodingField
                        {
                            Name = "VATCode",
                            Text = reader["VATCode"].ToString()
                        });

                        codingLines.Add(codingLine);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                conn.Close();
            }
            return codingLines;
        }

        private static Party GetSupplier(Document doc)
        {
            return doc.Parties.First(p => p.Type.ToLower().Equals("supplier"));
        }

        private static Party GetBuyer(Document doc)
        {
            return doc.Parties.First(p => p.Type.ToLower().Equals("buyer"));
        }


        public static bool Debug { get; set; }
        private static readonly Uri AuthUri = new Uri("https://services.readsoftonline.com/authentication/rest/authenticate");
        private static readonly Uri DocUri = new Uri("https://services.readsoftonline.com:443/documents/rest/");
        private static Cookie _currentCookie;
    }
}
