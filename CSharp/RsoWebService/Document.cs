﻿using System;
using System.Collections.Generic;

namespace RsoWebService
{
    internal class Document
    {
        public Guid Id;
        public string Version;
        public string OriginalFilename;
        public string Filename;
        public List<Party> Parties;
        public List<HeaderFields> HeaderFields;
        //public string Tables; //TODO Needs conversion
        //public string ProcessMessages; //TODO Needs conversion
        //public string SystemFields;
        public AccountingInformation AccountingInformation;
        public OutputResult OutputResult;

    }
}



//,"Tables":[
//    {"Type":"LineItem",
//     "TableColumns":[
//                {"Name":"LI_ArticleName","Type":"LIT_ArticleName","Format":"^[#$%&()+,\\-.\\\/0123456789:<>@[\\\\\\]_£º€AÀÂBCÇDEÉÈÊFGHIÎÏJKLMNOÓÒÔPQRSTUÙÛVWXYZaàâbcçdeéèêëfghiîïjklmnoóòôpqrstuùûüvwxyz  ]{0,200}$","DataType":"0"}
//                ,{"Name":"LI_TotalAmount","Type":"LIT_VatExcludedAmount","Format":".*","DataType":"1"}
//                ,{"Name":"LI_VAT%","Type":"LIT_VatRate","Format":"^[#$%&()+,\\-.\\\/0123456789:<>@[\\\\\\]_£º€]{1,2}([.,]{0,1}[#$%&()+,\\-.\\\/0123456789:<>@[\\\\\\]_£º€]{0,3}[%]{0,1})?$","DataType":"0"},
//                {"Name":"LI_VATAmount","Type":"LIT_VatAmount","Format":".*","DataType":"1"},
//                 {"Name":"LI_TransactionText","Type":"LI_TransactionText","Format":"^[#$%&()+,\\-.\\\/0123456789:<>@[\\\\\\]_£º€AÀÂBCÇDEÉÈÊFGHIÎÏJKLMNOÓÒÔPQRSTUÙÛVWXYZaàâbcçdeéèêëfghiîïjklmnoóòôpqrstuùûüvwxyz  ]{1,100}$","DataType":"0"}]
//        ,"TableRows":[]
//        }],
    
//"ProcessMessages":[],

//"SystemFields":[],

//{"Timestamp":"\/Date(1504784171000)\/","UserFullName":"<System>","Status":1,"Comment":"","EntryType":0,"Changes":"","Version":"","ChangeReportAvailable":false,"StatusAsInt":1},
//{"Timestamp":"\/Date(1504784176212)\/","UserFullName":"<System>","Status":36,"Comment":"","EntryType":2,"Changes":"","Version":"","ChangeReportAvailable":false,"StatusAsInt":36},
//{"Timestamp":"\/Date(1504784176943)\/","UserFullName":"<System>","Status":2,"Comment":"","EntryType":0,"Changes":"","Version":"documentextractioninprogress_sahdhdto1kwh2kfbxuxvdg","ChangeReportAvailable":false,"StatusAsInt":2},
//{"Timestamp":"\/Date(1504784177052)\/","UserFullName":"<System>","Status":3,"Comment":"","EntryType":0,"Changes":"","Version":"","ChangeReportAvailable":false,"StatusAsInt":3},
//{"Timestamp":"\/Date(1505120108373)\/","UserFullName":"User for Dynatos","Status":9,"Comment":"Starting workflow KQ test WF.","EntryType":0,"Changes":"Confirmed fields:\u000aCurrency=> Message: Uncertain value., Value: EUR","Version":"documentpendingmanualverification_xbxdt1s43kqcq7_0vr8iow","ChangeReportAvailable":false,"StatusAsInt":9}
//,{"Timestamp":"\/Date(1505120108497)\/","UserFullName":"<System>","Status":10,"Comment":"","EntryType":0,"Changes":"","Version":"","ChangeReportAvailable":false,"StatusAsInt":10},{"Timestamp":"\/Date(1505120111481)\/","UserFullName":"<System>","Status":11,"Comment":"","EntryType":0,"Changes":"","Version":"","ChangeReportAvailable":false,"StatusAsInt":11}],
//"TrackId":"20170907-7\/1","DocumentType":"BEL_PO","ValidationInfoCollection":[],"Origin":null}