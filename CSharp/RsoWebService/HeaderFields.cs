﻿namespace RsoWebService
{
    public class HeaderFields
    {
        public string Name;
        public string Type;
        public string Format;
        public string Text;
        public string ValidationError;
        public string Position;
        public string PageNumber;
        public string ValidationErrorMessage;
        public IsReadOnly IsReadOnly;
        public string DataType;
        public string TextDetail;
    }

    public class IsReadOnly
    {
        public string Value;
    }
}

//{"Name":"InvoiceNumber","Type":"invoicenumber","Format":".{1,100}","Text":"201725001","ValidationError":"0","Position":"163, 1424, 143, 22","PageNumber":"1","ValidationErrorMessage":null,"IsReadOnly":{"Value":false},"DataType":"0","TextDetail":null},
//{"Name":"InvoiceDate","Type":"invoicedate","Format":".*","Text":"20170719","ValidationError":"0","Position":"741, 1424, 145, 22","PageNumber":"1","ValidationErrorMessage":null,"IsReadOnly":{"Value":false},"DataType":"2","TextDetail":null},
//{"Name":"NetValue","Type":"invoicetotalvatexcludedamount","Format":".*","Text":"590.96","ValidationError":"0","Position":"2230, 2854, 77, 23","PageNumber":"1","ValidationErrorMessage":null,"IsReadOnly":{"Value":false},"DataType":"1","TextDetail":null},
//{"Name":"VatValue","Type":"invoicetotalvatamount","Format":".*","Text":"124.10","ValidationError":"0","Position":"2232, 2901, 75, 23","PageNumber":"1","ValidationErrorMessage":null,"IsReadOnly":{"Value":false},"DataType":"1","TextDetail":null},
//{"Name":"GrossValue","Type":"invoicetotalvatincludedamount","Format":".*","Text":"715.06","ValidationError":"0","Position":"2199, 2953, 108, 32","PageNumber":"1","ValidationErrorMessage":null,"IsReadOnly":{"Value":false},"DataType":"1","TextDetail":null},
//{"Name":"Currency","Type":"invoicecurrency","Format":"^[AÀÂBCÇDEÉÈÊFGHIÎÏJKLMNOÓÒÔPQRSTUÙÛVWXYZ]{3}$","Text":"EUR","ValidationError":"0","Position":"0, 0, 0, 0","PageNumber":"1","ValidationErrorMessage":null,"IsReadOnly":{"Value":false},"DataType":"0","TextDetail":null},
//{"Name":"PaymentId","Type":"PaymentId","Format":"^[#$%&()+,\\-.\\\/0123456789:<>@[\\\\\\]_£º€AÀÂBCÇDEÉÈÊFGHIÎÏJKLMNOÓÒÔPQRSTUÙÛVWXYZaàâbcçdeéèêëfghiîïjklmnoóòôpqrstuùûüvwxyz  ]{1,100}$","Text":"","ValidationError":"0","Position":"0, 0, 0, 0","PageNumber":"0","ValidationErrorMessage":null,"IsReadOnly":{"Value":false},"DataType":"0","TextDetail":null},
//{"Name":"TransactionText","Type":"TransactionText","Format":"^[#$%&()+,\\-.\\\/0123456789:<>@[\\\\\\]_£º€AÀÂBCÇDEÉÈÊFGHIÎÏJKLMNOÓÒÔPQRSTUÙÛVWXYZaàâbcçdeéèêëfghiîïjklmnoóòôpqrstuùûüvwxyz  ]{1,100}$","Text":"verzekering","ValidationError":"0","Position":"0, 0, 0, 0","PageNumber":"0","ValidationErrorMessage":null,"IsReadOnly":{"Value":false},"DataType":"0","TextDetail":null}]
