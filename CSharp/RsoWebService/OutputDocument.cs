﻿namespace RsoWebService
{
    internal class OutputDocument
    {
        public string DocumentUri;
        public string BatchId;
        public string BuyerId;
        public string ImageUri;
        public string ImagePageCount;
        public string DocumentId;
        public string OutputOperation;
        public string BatchExternalId;
        public string CompletionTime;
        public string Metadata;
        public string CustomerId;
        public string SmallThumbnailUri; 
    }
}