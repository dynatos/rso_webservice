﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace RsoWebService
{
    public class AccountingInformation
    {
        public List<CodingLine> CodingLines;
        public string ParkDocument;
        public string ErpCorrelationData;
        public string BaseType;
        public string Permalink;
        public History History;
    }

    public class CodingLine
    {
        public string AccountCode;
        public string Amount;
        public string CostCenterCode;
        public string ProjectNumber;
        public List<CodingField> CodingFields;
        public string AccountDetail;
        public string AccountType;

        public CodingLine()
        {
            CodingFields = new List<CodingField>();
        }
    }

    public class CodingField
    {
        public string Type;
        public string Text;
        public string Name;
        public string TextDetail;
        public string ShowAsSubLine;
    }
    //"AccountingInformation":{
    //    "CodingLines":[],
    //    "ParkDocument":false},
    //    "ErpCorrelationData":null,
    //    "BaseType":0,
    //    "Permalink":"https:\/\/storage-prodeu.readsoftonline.com\/viewer\/a82fcef118594c179fb0d238a58b904b\/5bd58bf9becce1494bd45cfe91da4291\/images\/e7bf164681ae4dd08f3ec09ecd956227",
    //   
    //},
}