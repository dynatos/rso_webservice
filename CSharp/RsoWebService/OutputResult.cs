﻿using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;

namespace RsoWebService
{
    internal class OutputResult
    {
        public int Status; //Failure = 0, Success = 1, Postpone = 2
        public string Message;
        public string CorrelationData;
        public List<CodingLine> CodingLines;
        public string ExternalId;
        public string ValidationInfoCollection;
    }
}